import React from "react";
import { shallow, mount } from "enzyme";
import Loading from "../components/Loading";

test("Loading component should show text prop", () => {
  const TEXT = "hello world";
  const loading = shallow(<Loading text={TEXT} />);

  expect(loading.text()).toEqual(TEXT);
});

test("Loading component should display default text when prop is undefined", () => {
  const loading = shallow(<Loading />);
  expect(loading.text()).toEqual("Loading");
});

test("Loading component should use speed prop as time interval", () => {
  const SPEED = 200;
  const loading = mount(<Loading text={"hello"} speed={SPEED} />);
  
  expect(loading.props().speed).toEqual(SPEED);
});

test("Loading component should use default speed when speed prop is undefined", () => {
  const loading = mount(<Loading />);

  expect(loading.props().speed).toEqual(300);
});

