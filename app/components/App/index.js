import React, { useState } from "react";
import Popular from "../Popular";
import Battle from "../Battle";
import NavButton from "../NavButton";
import "./styles.css";

export default function App() {
  const [showBattle, setShowBattle] = useState(false);

  const [user1, setUser1] = useState("");
  const [user2, setUser2] = useState("");
  const [githubInfo, setGithubInfo] = useState([]);
  const [showResult, setShowResult] = useState(false);
  const [loading, setLoading] = useState(false);

  function toggleView(showBattle) {
    setShowBattle(showBattle);
  }

  return (
    <div className="container">
      <div className="grid space-between">
        <NavButton
          isActive={!showBattle}
          text={"인기있는 저장소"}
          onClick={() => toggleView(false)}
        />
        <NavButton
          isActive={showBattle}
          text={"Github 배틀"}
          onClick={() => toggleView(true)}
        />
      </div>
      {!showBattle && <Popular />}
      {showBattle && <Battle
        user1={user1}
        setUser1={setUser1}
        user2={user2}
        setUser2={setUser2}
        githubInfo={githubInfo}
        setGithubInfo={setGithubInfo}
        showResult={showResult}
        setShowResult={setShowResult}
        loading={loading}
        setLoading={setLoading}
      />}
    </div>
  );
}
