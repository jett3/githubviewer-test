import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

const styles = {
  content: {
    fontSize: "35px",
    position: "absolute",
    left: "0",
    right: "0",
    marginTop: "20px",
    textAlign: "center",
  },
};

/*

  TODO: 아래 Loading 컴포넌트를 함수형 컴포넌트로 수정하고, `/spec/Loading.spec.js`에 테스트 내용을 보강하세요.

 */

export default function Loading({ text, speed }) {
  const [content, setContent] = useState(text ? text : Loading.defaultProps.text);
  const [tick, setTick] = useState(speed ? speed : Loading.defaultProps.speed);
  
  useEffect(() => {
    const interval = window.setInterval(() => {
      content === text + "..."
      ? setContent(text)
      : setContent(content + ".")
    }, tick);

    return function cleanup() {
      window.clearInterval(interval);
    }
  })

  return (
    <p style={styles.content}>{content}</p>
  );
}

Loading.propTypes = {
  text: PropTypes.string.isRequired,
  speed: PropTypes.number.isRequired,
};

Loading.defaultProps = {
  text: "Loading",
  speed: 300,
};
