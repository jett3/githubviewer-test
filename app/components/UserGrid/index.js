import React, { Fragment } from "react";
import "./style.css";

export default function UserGrid({ githubInfo }) {
  let count = 0;
  return (
    <ul className="result-container">
      {
        githubInfo.map((element) => {
          return (
            <Fragment>
              <li className="user-card" key={++count}>
                <p>{`${count}st Place is..`}</p> 
                <img className="avatar" src={element.profile.avatar_url}></img>
                <p>github username : {element.profile.login}</p>
                <p>score : {element.score}</p>
                <p>full name : {element.profile.name}</p>
                <p>location : {element.profile.location}</p>
                <p>following : {element.profile.following}</p>
                <p>followers : {element.profile.follwers}</p>
                <p>repositories : {element.profile.public_repos}</p>
              </li>
            </Fragment>
            
          )
        })
      }
    </ul>
  );
}