import React from "react";
import "./style.css";

export default function InputBar({ user1, user2, setUser1, setUser2 }) {
  return(
    <div className="input-container">
      <input 
        className="user-input"
        onChange={(e) => {setUser1(e.target.value)}} 
        value={user1}
      />
      <span className="user-input">vs</span>
      <input 
        className="user-input"
        onChange={(e) => {setUser2(e.target.value)}} 
        value={user2}
      />
    </div>
  );
}