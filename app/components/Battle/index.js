import React, { useState, useEffect } from "react";
import { battle } from "../../utils/api";
import Loading from "../Loading";
import InputBar from "../InputBar"
import UserGrid from "../UserGrid";
import "./style.css"

export default function Battle({ user1, setUser1, user2, setUser2, githubInfo, setGithubInfo, showResult, setShowResult, loading, setLoading}) {
  const [error, setError] = useState(null);

  function isCompleteInput() {
    return !(user1 === "" || user2 === "");
  }

  function loadGithubInfo (users) {
    if (users[0] === users[1]) {
      console.log("같은 아이디입니다.");
      return;
    }
    battle(users)
    .then((data) => {
      console.log(data);
      setGithubInfo(data);
      setShowResult(true);
      setLoading(false);
    })
    .catch((err) => {
      setError({error : "정보를 가져오는데 실패하였습니다"});
    })
  }

  function handleClick () {
    setLoading(true);
    loadGithubInfo([user1, user2]);
    setShowResult(true);
  }

  return (
    <div className="container">
      <h1 className="center-text">This is Battle!</h1>
      <InputBar 
        user1={user1}
        user2={user2}
        setUser1={setUser1}
        setUser2={setUser2}
      />
      <div className="input-container">
        {isCompleteInput() &&
          <button 
            className="battle-button"
            onClick={() => {handleClick()}}
          >Battle!</button>}
      </div>

      {loading && <Loading text="가져오는 중입니다" />}

      {showResult && 
      <UserGrid 
        githubInfo={githubInfo} />
      }
    </div>
  );
}
